import * as React from "react";
import { css } from "@emotion/css";
import { Paper } from "../../shared/Paper/Paper";
import { TextField } from "../../shared/TextField/TextField";
import { Button } from "../../shared/Button/Button";
import { observer } from "mobx-react";
import { mainStore } from "../../Stores/MainStore";
import { Loader } from "../../shared/Loader/Loader";

export const SecondView = observer(() => {
  React.useEffect(() => {
    mainStore.getUserInfo();
  }, []);
  return (
    <div className={secondViewStyle}>
      <Paper className={paperStyle}>
        {!mainStore.userData ? (
          <Loader />
        ) : (
          <div>
            <TextField
              label="First name"
              readOnly
              labelWidth={75}
              value={mainStore.userData.firstName}
            />
            <TextField
              label="Second name"
              readOnly
              labelWidth={75}
              value={mainStore.userData.secondName}
            />
            <TextField
              label="Birthday"
              readOnly
              labelWidth={75}
              value={mainStore.userData.birthday}
            />
            <TextField
              label="Balance"
              labelWidth={75}
              value={mainStore.userData.balance}
            />
            <div className={buttonContainer}>
              <Button onClick={() => mainStore.setStep(1)}>Back</Button>
              <Button onClick={() => mainStore.setStep(3)}>Next</Button>
            </div>
          </div>
        )}
      </Paper>
    </div>
  );
});

const secondViewStyle = css`
  height: 90vh;
  display: flex;
  justify-content: center;
  align-items: center;
`;

const buttonContainer = css`
  margin: 0 auto;
  display: flex;
  justify-content: space-around;
`;

const paperStyle = css`
  display: flex;
  justify-content: center;
  align-items: center;
  flex-direction: column;
  width: 400px;
  height: 400px;
  & div {
    margin-top: 10px;
  }
`;
