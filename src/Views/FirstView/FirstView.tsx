import * as React from "react";
import { Paper } from "../../shared/Paper/Paper";
import { TextField } from "../../shared/TextField/TextField";
import { Button } from "../../shared/Button/Button";
import { css } from "@emotion/css";
import { Keyboard } from "../../shared/Keyboard/Keyboard";
import { mainStore } from "../../Stores/MainStore";
import { observer } from "mobx-react";

export const FirstView = observer(() => {
  const [number, setNumber] = React.useState("");
  const [pin, setPin] = React.useState("");
  const [showKeyboard, setShowKeyboard] = React.useState(false);

  const changePin = (num: number) => {
    if (pin.length < 4) {
      setPin((prevState) => prevState + num);
    }
  };

  const clearLast = () => {
    setPin(pin.slice(0, -1));
  };

  const handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    setNumber(e.target.value);
  };
  const handleSubmit = React.useCallback(
    (e) => {
      e.preventDefault();
      if (pin.length < 4) {
        return;
      }
      mainStore.checkInfo({ number, pin });
    },
    [pin, number]
  );
  return (
    <div className={firstViewStyle}>
      <Paper>
        <form className={formStyle} onSubmit={handleSubmit}>
          <TextField
            label="Card number"
            onChange={handleChange}
            width={400}
            required
            type="number"
          />
          <div className={pinContainerStyle}>
            <TextField
              label="PIN"
              type="password"
              value={pin}
              readOnly
              required
            />
            <Button
              type="button"
              className={buttonStyle}
              onClick={() => setShowKeyboard(!showKeyboard)}
            >
              Show/hide keyboard
            </Button>
          </div>
          <Button>Next</Button>
        </form>
      </Paper>
      {mainStore.invalidUser && (
        <div className={errorStyle}>User not found</div>
      )}
      {mainStore.invalidPin && <div className={errorStyle}>Invalid pin</div>}
      {showKeyboard && <Keyboard onClick={changePin} onClear={clearLast} />}
    </div>
  );
});

const formStyle = css`
  height: 200px;
  width: 500px;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: space-around;
`;

const pinContainerStyle = css`
  display: flex;
`;

const firstViewStyle = css`
  height: 90vh;
  display: flex;
  justify-content: center;
  align-items: center;
  flex-direction: column;
`;

const buttonStyle = css`
  height: 100%;
  margin-left: 15px;
`;

const errorStyle = css`
  position: absolute;
  top: 60%;
`;
