import * as React from "react";
import { Paper } from "../../shared/Paper/Paper";
import { TextField } from "../../shared/TextField/TextField";
import { Button } from "../../shared/Button/Button";
import { css } from "@emotion/css";
import { mainStore } from "../../Stores/MainStore";
import { observer } from "mobx-react";

export const ThirdView = observer(() => {
  const [balance, setBalance] = React.useState("");
  const getCache = () => {
    mainStore.getCache(balance);
  };
  return (
    <div className={secondViewStyle}>
      <Paper className={paperStyle}>
        <TextField
          type="number"
          label="Enter sum"
          labelWidth={75}
          onChange={(e) => setBalance(e.target.value)}
        />
        <div className={buttonContainer}>
          <Button
            onClick={() => {
              mainStore.clearError();
              mainStore.setStep(2);
            }}
          >
            Back
          </Button>
          <Button onClick={getCache}>Get cache</Button>
        </div>
      </Paper>
      {mainStore.sumError && <div className={errorStyle}>Invalid sum</div>}
    </div>
  );
});

const secondViewStyle = css`
  height: 90vh;
  display: flex;
  justify-content: center;
  align-items: center;
  flex-direction: column;
`;

const buttonContainer = css`
  width: 70%;
  display: flex;
  justify-content: space-between;
`;

const paperStyle = css`
  display: flex;
  justify-content: center;
  align-items: center;
  flex-direction: column;
  width: 400px;
  height: 400px;
  & div {
    margin-top: 10px;
  }
`;
const errorStyle = css`
  position: absolute;
  top: 60%;
`;
