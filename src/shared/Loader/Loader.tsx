import * as React from "react";
import { css, keyframes } from "@emotion/css";

export const Loader = ({
  style,
}: Record<string, React.CSSProperties | undefined>) => (
  <div className={loaderStyle} style={style} />
);

const spin = keyframes`
  0% { transform: rotate(0deg); }
  100% { transform: rotate(360deg); }
`;

const loaderStyle = css`
  border: 2px solid #f3f3f3;
  border-top: 2px solid rgb(48, 96, 240);
  border-radius: 50%;
  width: 15px;
  height: 15px;
  animation: ${spin} 1s linear infinite;
`;
