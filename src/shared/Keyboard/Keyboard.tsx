import * as React from "react";
import { Button } from "../Button/Button";
import { css } from "@emotion/css";
export const Keyboard = ({
  onClick,
  onClear,
}: {
  onClick: (num: number) => void;
  onClear: () => void;
}) => {
  const renderKeyboard = () => {
    return [...Array(10).keys()].map((elem) => {
      return (
        <Button
          className={buttonStyle}
          key={elem}
          onClick={() => onClick(elem)}
        >
          {elem}
        </Button>
      );
    });
  };
  return (
    <div className={keyboardStyle}>
      {renderKeyboard()}
      <Button className={buttonStyle} onClick={onClear}>
        X
      </Button>
    </div>
  );
};

const keyboardStyle = css`
  position: absolute;
  display: flex;
  bottom: 150px;
`;

const buttonStyle = css`
  width: 40px;
  height: 40px;
`;
