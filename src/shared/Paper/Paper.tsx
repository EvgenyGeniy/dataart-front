import * as React from "react";
import "./paperStyles.scss";
import { cx } from "@emotion/css";

interface PaperProps {
  children: React.ReactNode;
  className?: string;
}
export const Paper: React.FC<PaperProps> = ({ children, className }) => {
  return (
    <div
      className={cx({
        paper: true,
        [className as string]: !!className,
      })}
    >
      {children}
    </div>
  );
};
