import * as React from "react";
import "./buttonStyle.scss";
import classNames from "classnames";

interface ButtonProps {
  children: HTMLElement | React.ReactNode;
  onClick?: () => void;
  type?: "button" | "reset" | "submit" | undefined;
  className?: string;
}

export const Button: React.FC<ButtonProps> = ({
  children,
  onClick,
  className,
  ...props
}) => {
  const buttonRef = React.useRef(document.createElement("button"));
  React.useEffect(
    () =>
      buttonRef.current.addEventListener(
        "click",
        (e: Partial<TouchEvent> & { clientX: number; clientY: number }) => {
          const event = e.touches ? e.touches[0] : e;
          const r = buttonRef.current.getBoundingClientRect(),
            d = Math.sqrt(Math.pow(r.width, 2) + Math.pow(r.height, 2)) * 2;
          buttonRef.current.style.cssText = `--s: 0; --o: 1;`;
          buttonRef.current.offsetTop;
          buttonRef.current.style.cssText = `--t: 1; --o: 0; --d: ${d}; --x:${
            event.clientX - r.left
          }; --y:${event.clientY - r.top};`;
        }
      ),
    []
  );
  return (
    <button
      ref={buttonRef}
      onClick={onClick}
      className={classNames({
        button: true,
        [className as string]: !!className,
      })}
      {...props}
    >
      {children}
    </button>
  );
};
