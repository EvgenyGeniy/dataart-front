import * as React from "react";
import "./textField.scss";
interface InputProps {
  value?: string;
  required?: boolean;
  label?: string;
  width?: number;
  onChange?: (e: React.ChangeEvent<HTMLInputElement>) => void;
  type?: string;
  labelWidth?: number;
  readOnly?: boolean;
}

export const TextField: React.FC<InputProps> = ({
  label,
  width,
  labelWidth,
  onChange,
  ...props
}) => {
  const style = {
    width: width && `${width}px`,
    borderRadius: label ? "" : "6px",
  };
  return (
    <div className="form-group" style={style}>
      {label && (
        <span
          style={{
            width: `${labelWidth}px`,
          }}
        >
          {label}
        </span>
      )}
      <input className="form-field" {...props} onChange={onChange} />
    </div>
  );
};
