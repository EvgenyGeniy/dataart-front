import { makeAutoObservable, observable } from "mobx";
import { Requests } from "../Requests";

class MainStore {
  @observable step = 1;
  @observable invalidUser = false;
  @observable invalidPin = false;
  @observable sumError = false;
  private uid = "";
  @observable userData: Record<string, string> = {};
  constructor() {
    makeAutoObservable(this);
  }
  public setStep(currentStep: number) {
    this.step = currentStep;
  }

  public async checkInfo({ number, pin }: { number: string; pin: string }) {
    try {
      this.invalidUser = false;
      this.invalidPin = false;
      const userData = await Requests.read(`/check?card=${number}&pin=${pin}`);
      this.uid = userData.data.id;
      this.setStep(2);
    } catch (e) {
      if (e.response.data === "User not found") {
        this.invalidUser = true;
      }
      if (e.response.data === "Invalid pin") {
        this.invalidPin = true;
      }
    }
  }

  public async getUserInfo() {
    const response = await Requests.read(`/info?id=${this.uid}`);
    this.userData = response.data.user;
  }

  public async getCache(balance: string) {
    try {
      this.sumError = false;
      await Requests.update(`/cache?id=${this.uid}`, {
        balance: balance,
      });
      this.setStep(2);
    } catch (e) {
      this.sumError = true;
    }
  }

  public clearError() {
    this.sumError = false;
  }
}

export const mainStore = new MainStore();
