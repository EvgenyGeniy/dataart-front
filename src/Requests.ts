import axios, { AxiosRequestConfig } from "axios";

const instance = axios.create({
  baseURL: "http://localhost:5000",
  headers: {
    "Access-Control-Allow-Origin": "*",
  },
});

class Request {
  public async create(url: string, body: Record<string, unknown> | string) {
    return await instance.post(url, body);
  }
  public async read(url: string, params?: AxiosRequestConfig) {
    return await instance.get(url, params);
  }
  public async update(url: string, body: Record<string, unknown> | string) {
    return await instance.put(url, body);
  }
  public async delete(url: string) {
    return await instance.delete(url);
  }
}

export const Requests = new Request();
