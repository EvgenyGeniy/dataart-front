import React from "react";
import "./App.css";
import { BrowserRouter, Redirect, Route, Switch } from "react-router-dom";
import { FirstView } from "./Views/FirstView/FirstView";
import { SecondView } from "./Views/SecondView/SecondView";
import { ThirdView } from "./Views/ThirdView/ThirdView";
import { observer } from "mobx-react";
import { mainStore } from "./Stores/MainStore";

export const Routes = observer(() => {
  return (
    <BrowserRouter>
      <Route exact path="/" component={FirstView} />
      <Route exact path="/info" component={SecondView} />
      <Route exact path="/complete" component={ThirdView} />
      <Switch>
        {mainStore.step === 1 && <Redirect to="/" />}
        {mainStore.step === 2 && <Redirect to="/info" />}
        {mainStore.step === 3 && <Redirect to="/complete" />}
      </Switch>
    </BrowserRouter>
  );
});
